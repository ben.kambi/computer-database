package com.company;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface CompanyRepository extends JpaRepository<Company, Long> {

	public List<Company> findByNameContains(String name);
	public List<Company> findByNameContainsOrderByNameAsc(String name);

}
